import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TestObject } from '../Objects/test-object';
import { environment } from 'src/environments/environment';

@Injectable({providedIn: 'root'})
export class TestObjectService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {}

  public getDuplicatesByColumn(column: string): Observable<TestObject[]> {
    return this.http.get<TestObject[]>(`${this.apiServerUrl}/testObject/duplicates/${column}`)
  }

  public getUniquesByColumn(column: string): Observable<TestObject[]> {
    return this.http.get<TestObject[]>(`${this.apiServerUrl}/testObject/uniques/${column}`)
  }
}
