import { Component, OnInit } from '@angular/core';
import { TestObject } from './Objects/test-object';
import { TestObjectService } from './Objects/test-object.service';
import { HttpErrorResponse } from '@angular/common/http';

interface Column {
  value: string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SunCodeFront';

  selectedFunction: string = 'Would you like to see Duplicates or Uniques?'
  selectedColumn: string = 'kolumna1';

  columns: Column[] = [
    { value: 'kolumna1' },
    { value: 'kolumna2' },
    { value: 'kolumna3' },
    { value: 'kolumna4' },
  ];

  displayedColumns: string[] = ['id', 'kolumna1', 'kolumna2', 'kolumna3', 'kolumna4'];
  dataSource: TestObject[] = [];



  constructor(private testObjectService: TestObjectService) { }

  public getDuplicatesByColumn(column: string): void {
    this.testObjectService.getDuplicatesByColumn(column).subscribe(
      (response: TestObject[]) => {
        this.selectedFunction = "Duplicates";
        this.dataSource = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public getUniquesByColumn(column: string): void {
    this.testObjectService.getUniquesByColumn(column).subscribe(
      (response: TestObject[]) => {
        this.selectedFunction = 'Uniques';
        this.dataSource = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }
}
